<?php

return array(
    'sucursal' => array(
        'precios' => '2,3,5,6,7,8'
    ),
    'meses'=>array(
        '1' => 'ENERO',
        '2' => 'FEBRERO',
        '3' => 'MARZO',
        '4' => 'ABRIL',
        '5' => 'MAYO',
        '6' => 'JUNIO',
        '7' => 'JULIO',
        '8' => 'AGOSTO',
        '9' => 'SEPTIEMBRE',
        '10' => 'OCTUBRE',
        '11' => 'NOVIEMBRE',
        '12' => 'DICIEMBRE'
    ),
    'años'=>array(
        '2013' => '2013',
        '2014' => '2014',
        '2015' => '2015',
        '2016' => '2016',
        '2017' => '2017',
        '2018' => '2018',
        '2019' => '2019',
        '2020' => '2020'
    ),

    'punto'=>array(
        'valor'=>'.10'
    ),

    'listado_puntos'=>array(
    '0' => 'Sin Puntos',
    '1' => 'Puntos Simples',
    '2' => 'Puntos Dobles',
    '3' => 'Puntos Triples',
    '4' => 'Puntos Cuadruples',
    '5' => 'Puntos Quintuples'

    )
);