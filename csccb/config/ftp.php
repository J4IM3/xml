<?php

return array(
    'starosa' => array(
        'ftp_server' => 'localhost',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => 'C:\\base\\xml\\starosa1\\RespaldoCFD\\filecfdi\\facturasxml',
        'carpeta_zips' => 'c:\\var\\www\\html\\zips\\starosa\\',
        'host' => 'localhost',
        'database' => 'c:\\base\\respaldo_sucursales\\STAROSA2016.FDB',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'merced' => array(
        'ftp_server' => 'provmercedtras.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/merced/',
        'carpeta_zips' => '/var/www/html/zips/merced/',
        'host' => 'provmercedtras.dyndns.org',
        'database' => 'c:\\base\\merced2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'xoxo' => array(
        'ftp_server' => 'provxoxo.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/xoxo/',
        'carpeta_zips' => '/var/www/html/zips/xoxo/',
        'host' => 'provxoxo.dyndns.org',
        'database' => 'c:\base\winvecajaxoxo2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'cedis' => array(
        //'ftp_server' => '187.174.253.195',
        'ftp_server' => 'autoservicio-rgmbkzrpqg.dynamic-m.com',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/cedis/',
        //'carpeta_local' => 'c:\\xampp\\htdocs\\xmls\\cedis\\',
        'carpeta_zips' => '/var/www/html/zips/cedis/',
        'host' => 'autoservicio-rgmbkzrpqg.dynamic-m.com',
        'database' => 'c:\base\winvecaja.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'auto' => array(
        'ftp_server' => 'autoservicio-rgmbkzrpqg.dynamic-m.com',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/cedis/',
        'carpeta_zips' => '/var/www/html/zips/cedis/',
        'host' => 'autoservicio-rgmbkzrpqg.dynamic-m.com',
        'database' => 'c:\base\winvecaja.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'central' => array(
        'ftp_server' => 'provabasto.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/central/',
        //'carpeta_local' => 'c:\\xampp\\htdocs\\xmls\\central\\',
        'carpeta_zips' => '/var/www/html/zips/central/',
        'host' => 'provabasto.dyndns.org',
        'database' => 'c:\base\central2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'puebla' => array(
        'ftp_server' => 'puebladigistar.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/puebla/',
        'carpeta_zips' => '/var/www/html/zips/puebla/',
        'host' => 'puebladigistar.dyndns.org',
        'database' => 'c:\\base\\winvecaja2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'matriz' => array(
        'carpeta_local' => 'C:\\base\\xml\\matriz\\',
        'carpeta_serie' => 'C:\\base\\xml\\matriz_serie\\',
        'host' => '172.20.16.234',
        'database' => 'C:\Users\Jaime\Desktop\winvecaja2016\winvecaja2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'soledad' => array(
        'ftp_server' => 'provsoledad.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/soledad/',
        'carpeta_zips' => '/var/www/html/zips/soledad/',
        'host' => 'provsoledad.dyndns.org',
        'database' => 'c:\base\soledad2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),
    'cedis_puebla' => array(
        'ftp_server' => 'provcedispuebla.dyndns.org',
        'ftp_user_name' => 'provesco',
        'ftp_user_pass' => 'provesco',
        'carpeta_local' => '/var/www/html/xmls/cedis_puebla/',
        'carpeta_zips' => '/var/www/html/zips/cedis_puebla/',
        'host' => 'provcedispuebla.dyndns.org',
        'database' => 'c:\\base\\winvecaja2016.fdb',
        'username' => 'SYSDBA',
        'password' => '050954ICA'
    ),


);