$(document).ready(function() {
    $('#example').DataTable({
        "language": {
            "search": "Buscar: ",
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Ningun resultado  - Lo siento",
            "info": "Mostrando página _PAGE_ de _PAGES_",
            "infoEmpty": "Ningun registro disponible",
            "infoFiltered": "(filtered from _MAX_ total records)",
            "paginate": {
                "first":    "Primero",
                "last":     "Último",
                "next":     "Siguiente",
                "previous": "Anterior"
            }
        }
    });
});