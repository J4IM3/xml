@extends('layouts/main')
@section('content')
    {!! Form::open(['route' => 'save-data', 'method' => 'POST']) !!}
        <div class="col-md-3 form-group">
            {!! Form::label('sucursal', 'Sucursal') !!}
            {!! Form::select('nombre', $sucursales,null,['class' => 'form-control' , 'required' => 'required']) !!}
        </div>

        <div class="col-md-3 form-group">
            {!! Form::label('Fecha ', 'Fecha inicio') !!}
            {!! Form::text('fecha_inicio',null,['class' => 'form-control' , 'required' => 'required','id'=>'fini']) !!}
        </div>

        <div class="col-md-3 form-group">
            {!! Form::label('Fecha ', 'Fecha fin') !!}
            {!! Form::text('fecha_fin',null,['class' => 'form-control','datepicker' , 'required' => 'required', 'id'=> 'ffin']) !!}
        </div>

        <div class="col-md-10">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </div>
    {!! Form::close() !!}
@endsection
@section('js')
<script>
   $(document).ready(function(){
        $( "#fini" ).datepicker();
        $( "#ffin" ).datepicker();
    });
</script>
@endsection
