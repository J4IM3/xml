<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CB Proveedora Escolar</title>
    @yield('css')
    @include('layouts/css')
    @include('layouts/js')
</head>
<body>
    @include('layouts/menu')
<div class="container">
    @yield('content')
</div>
@yield('js')
</body>
</html>