<?php
namespace App\Http\Repositories;
use App\Http\Entities\Sucursales;
use DB;
class SucursalRepo extends BaseRepo
{
    public function getModel()
    {
        return new Sucursales();
    }
}