<?php

namespace App\Http\Controllers;

use App\Http\Connections\FacturasFireRepo;
use App\Http\Repositories\SucursalRepo;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;

class InicioController extends Controller
{

    /**
     * @var FacturasFireRepo
     */
    private $facturasFireRepo;
    /**
     * @var SucursalRepo
     */
    private $sucursalRepo;

    public function __construct(FacturasFireRepo $facturasFireRepo,SucursalRepo $sucursalRepo)
    {

        $this->facturasFireRepo = $facturasFireRepo;
        $this->sucursalRepo = $sucursalRepo;
    }

    public function informacion(){
        $sucursales=$this->sucursalRepo->all()->pluck('nombre','bd');
        return view('buscar',compact('sucursales'));
    }

    public function saveData(Request $request){
        $input = $request->all();
        $fact = $this->facturasFireRepo->getFacturas($input['nombre'], $input['fecha_inicio'], $input['fecha_fin']);
        return $this->getXml($fact, $input['nombre']);
    }

    public function getXml($fact, $sucursal)
    {
        foreach ($fact  as $f){
            $file = Config::get('ftp.'.$sucursal.'.carpeta_local') . $f["numcfd"] . ".xml";
            $xml = $this->getInfoXml($file, $f['serie'], $f['folio']);

            copy($file , Config::get('ftp.'.$sucursal.'.carpeta_serie') . $f["numcfd"] . ".xml");
        }

        return Redirect::route('informacion');


    }

    public function getInfoXml($file, $serie = null, $folio=null){
        $info = array();
        if(file_exists($file)) {
            $xml_data = file_get_contents($file);
            if ($xml_data <> '') {
                $xml = simplexml_load_file($file);

                $ns = $xml->getNamespaces(true);
                $namespaces = $xml->getDocNamespaces(TRUE);
                $nodos = count($namespaces);
                $xml->registerXPathNamespace('c', $ns['cfdi']);
                if ($nodos > 3) {
                    $xml->registerXPathNamespace('t', $ns['tfd']);
                }
                foreach ($xml->xpath('//cfdi:Comprobante') as $cfdiComprobante) {
                    if($serie <> null) {
                        if (!isset($cfdiComprobante['serie'])) {
                            $xml->addAttribute('serie', $serie);
                        }else{
                            $cfdiComprobante['serie'] = $serie;
                        }
                    }
                    if($folio <> null) {
                        if (!isset($cfdiComprobante['folio'])) {
                            $xml->addAttribute('folio', $folio);
                        }else{
                            $cfdiComprobante['folio']=$folio;
                        }
                    }
                }
                $xml->asXML($file);

                return $xml;
            }
        }
    }
}
