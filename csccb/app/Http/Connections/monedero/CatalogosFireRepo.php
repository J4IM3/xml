<?php namespace Provesco\Connections\monedero;

use Provesco\Entities\Sucursal;


class CatalogosFireRepo extends \Firebird{

    public function getMaeart($sucursal='cedis'){
        $sql="select numart,nomart,uniart,numdep,flgart from maeart";
        return $this->get($sucursal,$sql);
    }

    public function getProart($sucursal='cedis'){
        $sql="select numart,numpro from proart";
        return $this->get($sucursal,$sql);
    }

    public function getMaepro($sucursal='cedis'){
        $sql="select numpro,nompro from maepro";
        return $this->get($sucursal,$sql);   
    }

   public function getDepartamentos($sucursal='cedis'){
        $sql="select numdep,nomdep,orddep from maedep order by numdep";
        return $this->get($sucursal,$sql);
    }

    public function setClienteSucursales($datos,$sucursal){

        $numcte = $datos->nocliente;
        $nomcte = $datos->nombre;
        $rfc = $datos->rfc;
        $direccion = $datos->direccion;
        $colonia = $datos->colonia;
        $municipio = $datos->municipio;
        $poblacion = $datos->poblacion;
        $codigo_postal = $datos->codigo_postal;
        $no_interior = $datos->no_interior;
        $no_exterior = $datos->no_exterior;
        $estado = $datos->estado;
        $pais = $datos->pais;
        $telefono = $datos->telefono;
        $correo = $datos->correo;

        $sql="insert into maecte(numcte,nomcte,dircte,colcte,pobcte,estado,pais,telcte,mailcte,muncte,nlecte,nlicte,cpcte,numtipo,numruta,rfccte)
        values('$numcte','$nomcte','$direccion','$colonia','$poblacion','$estado','$pais','$telefono','$correo','$municipio','$no_exterior',
        '$no_interior','$codigo_postal','0','0','$rfc')";
        foreach ($sucursal as $suc){ 
        $this->set($suc->bd,$sql);    
        }        
    }

    public function updateClienteSucursales($datos,$sucursal){
        $numcte = $datos->nocliente;
        $nomcte = $datos->nombre;
        $rfc = $datos->rfc;
        $direccion = $datos->direccion;
        $colonia = $datos->colonia;
        $municipio = $datos->municipio;
        $poblacion = $datos->poblacion;
        $codigo_postal = $datos->codigo_postal;
        $no_interior = $datos->no_interior;
        $no_exterior = $datos->no_exterior;
        $estado = $datos->estado;
        $pais = $datos->pais;
        $telefono = $datos->telefono;
        $correo = $datos->correo;
        $sql="update maecte set nomcte = '$nomcte',dircte='$direccion',colcte = '$colonia',pobcte='$poblacion',estado='$estado',pais='$pais',
        telcte ='$telefono', mailcte ='$correo',muncte='$municipio',nlecte='$no_exterior',nlicte='$no_interior',cpcte = '$codigo_postal', 
        rfccte = '$rfc' where numcte = '$numcte'"; 
        foreach ($sucursal as $suc){ 
        $this->set($suc->bd,$sql);    
        }  

    }



}