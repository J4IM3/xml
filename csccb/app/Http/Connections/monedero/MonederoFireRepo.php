<?php namespace Provesco\Connections\monedero;

class MonederoFireRepo extends \Firebird{


   public function getMovimiento($sucursal, $serie, $folio){
       $sql = "select c.nummov,
               SUBSTR(FLGMOV,2,2) as activo,
               aplimov, fcapmov, numcte, horamov
               from maemovca02 c
               inner join movcte mv on mv.nummov = c.nummov
               where serfol = '$serie' and numfol = $folio";
       return $this->get($sucursal, $sql);

   }

    public function getDetalle($sucursal, $nummov){
        $sql = "select numart, valdtm from maedtma02 where nummov = $nummov";
        return $this->get($sucursal, $sql);
    }

}