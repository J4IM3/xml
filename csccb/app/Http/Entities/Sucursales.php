<?php

namespace App\Http\Entities;
use Illuminate\Database\Eloquent\Model;

class Sucursales extends Model
{
    protected $fillable = ['nombre','bd'];
    protected $table = 'sucursales';
}