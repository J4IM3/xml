<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::group(array('prefix'=>'api'),function(){
    Route::get('/',['as'=>'index','uses'=>'InicioController@index']);

});
Route::get('sistema',['as'=>'sistema','uses'=>'InicioController@index']);
Route::get('informacion',['as'=>'informacion','uses'=>'InicioController@informacion']);
Route::post('save-data',['as'=>'save-data','uses'=>'InicioController@saveData']);

//Route::get('/test', ['uses'=>'InicioController@index']);